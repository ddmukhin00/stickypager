package com.fairnut.stickypager.pager

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FiniteAnimationSpec
import androidx.compose.animation.core.VisibilityThreshold
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.with
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PagerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.mapSaver
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntOffset
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@ExperimentalFoundationApi
@ExperimentalAnimationApi
@Stable
internal class StickyPagerDelegate(
    initialPage: Int,
    initialScrollStates: Map<Int, LazyListState> = mapOf(),
) {
    private val scrollStates: MutableMap<Int, LazyListState> = initialScrollStates.toMutableMap()

    private val pagerState: PagerState = PagerState(
        initialPage = initialPage,
    )

    private val currentPage by derivedStateOf { pagerState.currentPage }

    private val targetPage by derivedStateOf { pagerState.targetPage }

    val currentScrollState by derivedStateOf {
        scrollStates.getOrElse(pagerState.currentPage) {
            LazyListState()
        }
    }

    init {
        if (scrollStates[initialPage] == null) {
            scrollStates[initialPage] = LazyListState()
        }
    }

    private suspend fun selectPage(
        animationSpec: FiniteAnimationSpec<Float>,
        topHeight: Int,
        page: Int,
    ) {
        val nextScrollState = scrollStates[page]

        if (page == currentPage) {
            return
        }

        val range = if (page > currentPage) {
            currentPage + 1..page
        } else {
            currentPage - 1 downTo page
        }

        if (currentScrollState.inTop(topHeight)) {
            // мы в top content
            for (i in range) {
                scrollStateToOffset(i, 0, currentScrollState.firstVisibleItemScrollOffset)
            }
        } else if (nextScrollState == null ||
            nextScrollState.inTop(topHeight)
        ) {
            for (i in range) {
                scrollStateToOffset(i, 0, topHeight)
            }
        }

        pagerState.animateScrollToPage(
            page = page,
            animationSpec = animationSpec,
        )
    }

    private fun getScrollStateByIndex(page: Int) = scrollStates.getOrElse(page) {
        LazyListState()
    }

    private suspend fun scrollStateToOffset(page: Int, index: Int, offset: Int) {
        if (scrollStates[page] == null) {
            scrollStates[page] = LazyListState(
                firstVisibleItemScrollOffset = offset,
            )
        } else {
            scrollStates[page]?.scrollToItem(
                index = index,
                scrollOffset = offset,
            )
        }
    }

    private fun LazyListState.inTop(topHeight: Int): Boolean =
        firstVisibleItemIndex == 0 && firstVisibleItemScrollOffset < topHeight

    @Composable
    fun ListContent(
        size: Int,
        key: (Int) -> Any,
        content: @Composable (Int, LazyListState) -> Unit,
    ) {
        HorizontalPager(
            pageCount = size,
            state = pagerState,
            userScrollEnabled = false,
            key = { key(it) },
            verticalAlignment = Alignment.Top,
        ) {
            content(it, getScrollStateByIndex(it))
        }
    }

    @Composable
    fun TopContent(
        enableTopAnimation: Boolean,
        animationSpec: FiniteAnimationSpec<IntOffset>,
        content: @Composable (Int) -> Unit,
    ) {
        val transitionSpec: AnimatedContentScope<Int>.() -> ContentTransform = remember {
            {
                val newIndex = targetState
                val initialIndex = initialState

                if (newIndex > initialIndex) {
                    slideInHorizontally(animationSpec) { it } with
                        slideOutHorizontally(animationSpec) { -it }
                } else {
                    slideInHorizontally(animationSpec) { -it } with
                        slideOutHorizontally(animationSpec) { it }
                }
            }
        }

        if (enableTopAnimation) {
            AnimatedContent(
                targetState = targetPage,
                transitionSpec = transitionSpec,
                label = "TopContentAnimation",
            ) {
                content(it)
            }
        } else {
            content(currentPage)
        }
    }

    @Composable
    fun HeaderContent(
        size: Int,
        coroutineScope: CoroutineScope,
        animationSpec: FiniteAnimationSpec<Float>,
        topHeight: Int,
        pageContent: @Composable BoxScope.(Int) -> Unit,
        onSelect: (Int) -> Unit,
        content: @Composable ((Int, @Composable (Modifier) -> Unit) -> Unit)?,
    ) {
        val pagerHeader: @Composable (Modifier) -> Unit = {
            StickyPagerHeader(
                modifier = it,
                size = size,
                currentPage = targetPage,
                animationSpec = animationSpec,
                pageContent = pageContent,
                onSelect = { page ->
                    onSelect(page)
                    coroutineScope.launch {
                        selectPage(animationSpec, topHeight, page)
                    }
                },
            )
        }

        if (content == null) {
            pagerHeader(Modifier)
        } else {
            content(currentPage) {
                pagerHeader(it)
            }
        }
    }

    companion object {
        private const val CURRENT_PAGE_KEY = "CURRENT_PAGE_KEY"
        private const val FIRST_VISIBLE_ITEM_KEY = "FIRST_VISIBLE_ITEM_KEY"
        private const val FIRST_VISIBLE_ITEM_OFFSET_KEY = "FIRST_VISIBLE_ITEM_OFFSET_KEY"

        // save only currentpage
        internal val Saver: Saver<StickyPagerDelegate, *> = mapSaver(
            save = {
                mapOf(
                    CURRENT_PAGE_KEY to it.pagerState.currentPage,
                    FIRST_VISIBLE_ITEM_KEY to (
                        it.scrollStates[it.currentPage]?.firstVisibleItemIndex
                            ?: 0
                        ),
                    FIRST_VISIBLE_ITEM_OFFSET_KEY to (
                        it.scrollStates[it.currentPage]?.firstVisibleItemScrollOffset
                            ?: 0
                        ),
                )
            },
            restore = {
                val initialPage = it[CURRENT_PAGE_KEY] as Int

                StickyPagerDelegate(
                    initialPage = initialPage,
                    initialScrollStates = mutableMapOf(
                        initialPage to LazyListState(
                            firstVisibleItemIndex = it[FIRST_VISIBLE_ITEM_KEY] as Int,
                            firstVisibleItemScrollOffset = it[FIRST_VISIBLE_ITEM_OFFSET_KEY] as Int,
                        ),
                    ),
                )
            },
        )
    }
}

@Immutable
sealed interface AnimationSpec {

    val floatSpec: FiniteAnimationSpec<Float>
    val offsetSpec: FiniteAnimationSpec<IntOffset>

    class Tween(durationMillis: Int) : AnimationSpec {
        override val floatSpec: FiniteAnimationSpec<Float> =
            androidx.compose.animation.core.tween(durationMillis)
        override val offsetSpec: FiniteAnimationSpec<IntOffset> =
            androidx.compose.animation.core.tween(durationMillis)
    }

    class Spring(
        stiffness: Float = androidx.compose.animation.core.Spring.StiffnessMediumLow,
    ) : AnimationSpec {
        override val floatSpec: FiniteAnimationSpec<Float> = androidx.compose.animation.core.spring(
            stiffness = stiffness,
            visibilityThreshold = 0.01f,
        )
        override val offsetSpec: FiniteAnimationSpec<IntOffset> =
            androidx.compose.animation.core.spring(
                stiffness = stiffness,
                visibilityThreshold = IntOffset.VisibilityThreshold,
            )
    }
}
