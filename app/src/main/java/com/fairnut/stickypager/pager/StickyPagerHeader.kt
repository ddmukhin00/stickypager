package com.fairnut.stickypager.pager

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.constrain
import androidx.compose.ui.unit.constrainHeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.offset

@ExperimentalFoundationApi
@Composable
internal fun StickyPagerHeader(
    modifier: Modifier = Modifier,
    size: Int,
    currentPage: Int,
    pageContent: @Composable BoxScope.(Int) -> Unit,
    backgroundColor: Color = Color.White,
    activeColor: Color = Color(0xFFF8F7F7),
    height: Dp = 52.dp,
    padding: Dp = 2.dp,
    animationSpec: AnimationSpec<Float>,
    onSelect: (Int) -> Unit,
) {
    val index by animateFloatAsState(
        animationSpec = animationSpec,
        targetValue = currentPage.toFloat(),
        label = "ScrollPagerHeaderValue",
    )

    Layout(
        modifier = modifier,
        content = {
            // background
            Box(
                Modifier
                    .background(
                        color = backgroundColor,
                        shape = RoundedCornerShape(12.dp),
                    ),
            )
            // pages
            repeat(size) {
                Box(
                    Modifier
                        .clickable(
                            interactionSource = MutableInteractionSource(),
                            indication = null,
                            onClick = { onSelect(it) },
                        ),
                ) {
                    pageContent(it)
                }
            }
            // active page
            Box(
                Modifier
                    .background(
                        color = activeColor,
                        shape = RoundedCornerShape(12.dp),
                    ),
            )
        },
    ) { measurables, constraints ->
        val width = constraints.maxWidth.toFloat() / size.coerceAtLeast(1)
        val paddingValue = padding.roundToPx()

        val bg = measurables.first().measure(
            constraints.constrain(
                Constraints.fixed(
                    constraints.maxWidth,
                    height.roundToPx(),
                ),
            ),
        )
        val elements = measurables.subList(1, measurables.lastIndex).map {
            it.measure(
                constraints.constrain(Constraints.fixed(width.toInt(), height.roundToPx())),
            )
        }
        val active = measurables.last().measure(
            constraints.constrain(
                Constraints.fixed(width.toInt(), height.roundToPx())
                    .offset(
                        horizontal = -2 * paddingValue,
                        vertical = -2 * paddingValue,
                    ),
            ),
        )
        var x = 0

        layout(
            constraints.maxWidth,
            constraints.constrainHeight(
                height.roundToPx(),
            ),
        ) {
            bg.place(
                x = 0,
                y = 0,
            )
            if (size > 0) {
                active.placeWithLayer(
                    x = paddingValue,
                    y = paddingValue,
                ) {
                    translationX = width * index
                }
            }
            elements.forEach {
                it.place(
                    x = x,
                    y = 0,
                )
                x += it.width
            }
        }
    }
}