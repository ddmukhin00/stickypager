package com.fairnut.stickypager.pager

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * Overload for [HorizontalStickyPager]
 * @param pages typed list of pages
 * @param placeholder placeholder, if your pages are loading asynchronously, you need placeholder to show while pages
 * list is empty. If you need to load contents of pager sequentially (topContent and pages header background -> pages header
 * content and list content) use overload of Pager which accepts size.
 *
 * Recommended to use when your pages are loading immediately or fast enough (not using api calls).
 */
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Composable
fun <T : Any> HorizontalStickyPager(
    pages: List<T>,
    modifier: Modifier = Modifier,
    initialPage: T = pages.first(),
    placeholder: @Composable () -> Unit,
    key: (Int, T) -> Any = { index, _ -> index },
    pageContent: @Composable BoxScope.(T) -> Unit = { },
    onSelect: (T) -> Unit = {},
    animationSpec: AnimationSpec = remember { AnimationSpec.Tween(500) },
    enableTopAnimation: Boolean = false,
    contentPadding: (T) -> PaddingValues = { PaddingValues(0.dp) },
    topContent: @Composable ((T) -> Unit)? = null,
    header: @Composable ((T, @Composable (Modifier) -> Unit) -> Unit)? = null,
    content: LazyListScope.(T) -> Unit,
) {
    if (pages.isEmpty()) {
        placeholder()
    } else {
        HorizontalStickyPager(
            size = pages.size,
            modifier = modifier,
            initialPage = pages.indexOf(initialPage).coerceAtLeast(0),
            key = { key(it, pages[it]) },
            pageContent = {
                pageContent(pages[it])
            },
            onSelect = { onSelect(pages[it]) },
            animationSpec = animationSpec,
            enableTopAnimation = enableTopAnimation,
            contentPadding = {
                contentPadding(pages[it])
            },
            topContent = topContent?.let {
                {
                    topContent(pages[it])
                }
            },
            header = header?.let {
                { index, pagerHeader ->
                    header(pages[index], pagerHeader)
                }
            },
            content = {
                content(pages[it])
            },
        )
    }
}

/**
 * A pager that scrolls horizontally with sticky header. Sticky header is defined in [StickyPagerHeader].
 *
 *
 * @param size count of pages
 * @param initialPage initially selected page
 * @param key key for pages, must be unique
 * @param pageContent content of pages header by index
 * @param modifier a modifier instance to be applied to this Pager outer layout
 * @param onSelect callback, which triggers when new page is selected
 * @param animationSpec [AnimationSpec], by which all StickyPager contents will animate.
 * @param enableTopAnimation if should enable animation of top content. Should enable when
 * topContent is different depending on current page
 * @param contentPadding a padding around the whole content. This will add padding for the
 * content after it has been clipped, which is not possible via [modifier] param.
 * @param topContent top content that is shown above header by initial and then hides, when
 *  scrolled
 * @param header header lambda which accepts current page and sticky header composable as params.
 * By this, you can customize sticky header with modifier and other composables
 * @param content content with [LazyListScope]
 * @see [StickyPagerDelegate]
 */

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@Composable
fun HorizontalStickyPager(
    size: Int,
    modifier: Modifier = Modifier,
    initialPage: Int = 0,
    key: (Int) -> Any = { it },
    pageContent: @Composable BoxScope.(Int) -> Unit = { },
    onSelect: (Int) -> Unit = {},
    animationSpec: AnimationSpec = remember { AnimationSpec.Tween(500) },
    enableTopAnimation: Boolean = false,
    contentPadding: (Int) -> PaddingValues = { PaddingValues(0.dp) },
    topContent: @Composable ((Int) -> Unit)? = null,
    header: @Composable ((Int, @Composable (Modifier) -> Unit) -> Unit)? = null,
    content: LazyListScope.(Int) -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()

    val delegate = rememberSaveable(
        size,
        initialPage,
        saver = StickyPagerDelegate.Saver,
    ) {
        StickyPagerDelegate(
            initialPage = initialPage,
        )
    }

    SubcomposePagerLayout(
        modifier = modifier,
        lazyListState = delegate.currentScrollState,
        listContent = { offset ->
            delegate.ListContent(
                size = size,
                key = key,
            ) { page, scrollState ->
                LazyColumn(
                    state = scrollState,
                    contentPadding = contentPadding(page),
                ) {
                    item {
                        Spacer(Modifier.height(offset))
                    }
                    content(page)
                }
            }
        },
        topContent = topContent?.let { top ->
            {
                delegate.TopContent(enableTopAnimation, animationSpec.offsetSpec) {
                    top(it)
                }
            }
        },
        headerContent = { topHeight ->
            delegate.HeaderContent(
                size = size,
                topHeight = topHeight,
                coroutineScope = coroutineScope,
                onSelect = onSelect,
                animationSpec = animationSpec.floatSpec,
                pageContent = pageContent,
                content = header,
            )
        },
    )
}

@Composable
private fun SubcomposePagerLayout(
    modifier: Modifier = Modifier,
    lazyListState: LazyListState,
    topContent: @Composable (() -> Unit)? = null,
    listContent: @Composable (offset: Dp) -> Unit,
    headerContent: @Composable (topHeight: Int) -> Unit,
) {
    SubcomposeLayout(modifier) { constraints ->
        val top = topContent?.let {
            subcompose(PagerSlot.Top) {
                it()
            }.map { it.measure(constraints) }.takeIf { it.isNotEmpty() }
        }?.first()

        val header = subcompose(PagerSlot.Header) {
            headerContent(top?.height ?: 0)
        }.map { it.measure(constraints) }.first()

        val offset = header.height + (top?.height ?: 0)

        val list = subcompose(PagerSlot.List) {
            listContent(offset.toDp())
        }.map { it.measure(constraints) }.first()

        val scroll = lazyListState.firstVisibleItemScrollOffset +
            (lazyListState.firstVisibleItemIndex * offset)
        var height = -scroll

        layout(
            constraints.maxWidth,
            constraints.maxHeight,
        ) {
            list.place(
                x = 0,
                y = 0,
            )
            top?.place(
                x = 0,
                y = height,
            )
            height += top?.height ?: 0
            header.place(
                x = 0,
                y = height.coerceAtLeast(0),
            )
        }
    }
}

private enum class PagerSlot { List, Top, Header }
