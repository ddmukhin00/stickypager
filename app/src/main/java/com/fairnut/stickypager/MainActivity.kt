package com.fairnut.stickypager

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.fairnut.stickypager.pager.HorizontalStickyPager
import com.fairnut.stickypager.theme.typography

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalAnimationApi::class, ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Box(Modifier.fillMaxSize().background(Color(0xFFF2F1F0))) {
                HorizontalStickyPager(
                    size = 2,
                    pageContent = {
                        Text(
                            modifier = Modifier.align(Alignment.Center),
                            text = if (it == 0) "Первый" else "Второй",
                            style = typography.bodyMedium,
                        )
                    },
                    contentPadding = { PaddingValues(horizontal = 12.dp) },
                    header = { _, pagerHeader ->
                        pagerHeader(
                            Modifier.padding(
                                start = 12.dp,
                                bottom = 8.dp,
                                end = 12.dp,
                            ),
                        )
                    },
                    topContent = {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentHeight()
                                .padding(
                                    vertical = 16.dp,
                                    horizontal = 24.dp,
                                ),
                        ) {
                            Text(
                                text = "Заголовок",
                                modifier = Modifier
                                    .align(Alignment.Center),
                                style = typography.headlineLarge,
                            )
                        }
                    },
                ) {
                    if (it == 0) {
                        items(30) {
                            Item("Я первый!", height = 80.dp)
                            Spacer(Modifier.height(8.dp))
                        }
                    } else {
                        items(20) {
                            Item("Я второй!", height = 160.dp)
                            Spacer(Modifier.height(8.dp))
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun Item(
    value: String,
    height: Dp,
) {
    Box(
        Modifier
            .fillMaxWidth()
            .height(height)
            .background(
                color = Color.White,
                shape = RoundedCornerShape(16.dp),
            ),
    ) {
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = value,
            style = typography.labelMedium,
        )
    }
}
